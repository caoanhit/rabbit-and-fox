%YAML 1.1
%TAG !u! tag:unity3d.com,2011:
--- !u!319 &31900000
AvatarMask:
  m_ObjectHideFlags: 0
  m_PrefabParentObject: {fileID: 0}
  m_PrefabInternal: {fileID: 0}
  m_Name: Aim
  m_Mask: 01000000010000000100000001000000010000000100000001000000010000000100000001000000010000000100000001000000
  m_Elements:
  - m_Path: 
    m_Weight: 1
  - m_Path: Armature
    m_Weight: 0
  - m_Path: Armature/Torso
    m_Weight: 0
  - m_Path: Armature/Torso/Belly
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Neck
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Neck/Head
    m_Weight: 1
  - m_Path: Armature/Torso/Belly/Chest/Neck/Head/Ear1_l
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Neck/Head/Ear1_l/Ear2_l
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Neck/Head/Ear1_r
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Neck/Head/Ear1_r/Ear2_r
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_l
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_l/UpperArm_l
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_l/UpperArm_l/Arm_l
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_l/UpperArm_l/Arm_l/Hand_l
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_l/UpperArm_l/Arm_l/Hand_l/Finger1_l
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_l/UpperArm_l/Arm_l/Hand_l/Finger1_l/Finger2_l
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_l/UpperArm_l/Arm_l/Hand_l/Finger1_l/Finger2_l/Finger3_l
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_l/UpperArm_l/Arm_l/Hand_l/Thumb1_l
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_l/UpperArm_l/Arm_l/Hand_l/Thumb1_l/Thumb2_l
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_l/UpperArm_l/Arm_l/Hand_l/Thumb1_l/Thumb2_l/Thumb3_l
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_r
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_r/UpperArm_r
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_r/UpperArm_r/Arm_r
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_r/UpperArm_r/Arm_r/Hand_r
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_r/UpperArm_r/Arm_r/Hand_r/Finger1_r
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_r/UpperArm_r/Arm_r/Hand_r/Finger1_r/Finger2_r
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_r/UpperArm_r/Arm_r/Hand_r/Finger1_r/Finger2_r/Finger3_r
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_r/UpperArm_r/Arm_r/Hand_r/Thumb1_r
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_r/UpperArm_r/Arm_r/Hand_r/Thumb1_r/Thumb2_r
    m_Weight: 0
  - m_Path: Armature/Torso/Belly/Chest/Shoulder_r/UpperArm_r/Arm_r/Hand_r/Thumb1_r/Thumb2_r/Thumb3_r
    m_Weight: 0
  - m_Path: Armature/Torso/Thigh_l
    m_Weight: 0
  - m_Path: Armature/Torso/Thigh_l/Leg_l
    m_Weight: 0
  - m_Path: Armature/Torso/Thigh_l/Leg_l/Foot_l
    m_Weight: 0
  - m_Path: Armature/Torso/Thigh_l/Leg_l/Foot_l/Toe_l
    m_Weight: 0
  - m_Path: Armature/Torso/Thigh_r
    m_Weight: 0
  - m_Path: Armature/Torso/Thigh_r/Leg_r
    m_Weight: 0
  - m_Path: Armature/Torso/Thigh_r/Leg_r/Foot_r
    m_Weight: 0
  - m_Path: Armature/Torso/Thigh_r/Leg_r/Foot_r/Toe_r
    m_Weight: 0
  - m_Path: Cube
    m_Weight: 0
