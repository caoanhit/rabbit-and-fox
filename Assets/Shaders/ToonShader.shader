﻿Shader "ToonShader"
{
	Properties
	{
		[NoScaleOffset] _MainTex("Texture", 2D) = "white" {}
		[NoScaleOffset] _Gradient("Gradient", 2D) = "white" {}
	}
		SubShader
	{
		Tags{ "LightMode" = "ForwardBase" }

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog

			#include "UnityCG.cginc"

			struct appdata
			{
				float4 pos : POSITION;
				float4 normal : NORMAL;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				UNITY_FOG_COORDS(1)
				float4 pos : SV_POSITION;
				fixed3 color : COLOR0;
			};
			sampler2D _MainTex;
			sampler2D _Gradient;

			v2f vert(appdata v)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(v.pos);
				o.uv = float4(v.uv.xy, 0, 0);
				half3 worldNormal = UnityObjectToWorldNormal(v.normal);
				o.color = max(0.005, dot(worldNormal, _WorldSpaceLightPos0.xyz));
				UNITY_TRANSFER_FOG(o,o.pos);
				return o;
			}

			fixed4 frag(v2f i) : SV_Target
			{
			// sample the texture
			fixed4 col = tex2D(_Gradient,i.color) * tex2D(_MainTex,i.uv);
			// apply fog
			UNITY_APPLY_FOG(i.fogCoord, col);
			return col;
		}
		ENDCG
	}
		UsePass "Legacy Shaders/VertexLit/SHADOWCASTER"
	}
}
