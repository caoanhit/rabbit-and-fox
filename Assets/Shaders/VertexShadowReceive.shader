﻿Shader "VertexShadowReceive"
{
	Properties
	{
	}
	SubShader
	{
		Tags{ "RenderType" = "Opaque" "LightMode" = "ForwardBase" }
		LOD 80
		Pass
		{
			CGPROGRAM
		#pragma vertex vert
		#pragma fragment frag
		#pragma multi_compile_fwdbase
			// make fog work
		#pragma multi_compile_fog


		#include "UnityCG.cginc"
		#include "AutoLight.cginc"

			struct appdata
		{
			float4 vertex : POSITION;
			fixed4 color : COLOR;
		};

		struct v2f
		{
			SHADOW_COORDS(1)
			UNITY_FOG_COORDS(2)
			float4 vertex : SV_POSITION;
			fixed4 color : COLOR;
		};

		v2f vert(appdata v)
		{
			v2f o;
			o.vertex = UnityObjectToClipPos(v.vertex);
			o.color = v.color;
			TRANSFER_SHADOW(o);
			UNITY_TRANSFER_FOG(o,o.vertex);
			return o;
		}

		fixed4 frag(v2f i) : SV_Target
		{
			// sample the texture
			fixed atten = SHADOW_ATTENUATION(i);
			fixed4 col = i.color*atten;
			// apply fog
			UNITY_APPLY_FOG(i.fogCoord, col);
			return col;
		}
		ENDCG
		}
	}
}
