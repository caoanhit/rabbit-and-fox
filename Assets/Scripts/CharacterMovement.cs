﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class CharacterMovement : MonoBehaviour {
    public float dashSpeed;
    public float dashTime;
    public float rotSpeed;
    public float knockbackTime;
    public Vector3 frontrayoffset;
    public Vector3 backrayoffset;
    public ParticleSystem dashparticles;
    public float particleSize;
    public float particleOffset;
    public ParticleSystem hitparticle;

    bool Dashing=false;
    IEnumerator IMove;
    Rigidbody rb;
    Animator anim;
    Vector3 previouspoint;
    bool DashRight;
    Ray frontray, backray, groundray;
    bool ground=true;
	// Use this for initialization
	void Start () {
        frontray = new Ray(transform.position + frontrayoffset, Vector3.down);
        backray = new Ray(transform.position + backrayoffset, Vector3.down);
        groundray = new Ray(transform.position +Vector3.up*0.1f, Vector3.down);
        anim = GetComponent<Animator>();
        rb = GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
    public void Dash(Vector3 point)
    {
        
        if (!Dashing&& ground)
        {
            var emitParams = new ParticleSystem.EmitParams();
            emitParams.position = transform.position+point*particleOffset+ Vector3.up * 0.5f;
            emitParams.rotation3D = Quaternion.LookRotation(point).eulerAngles;
            emitParams.startSize = point.magnitude * particleSize;
            dashparticles.Emit(emitParams, 1);
            IMove = IDash(point,dashTime, dashSpeed, rotSpeed);
            StartCoroutine(IMove);
        }
    }
    void Knockback()
    {
        anim.SetBool("Knockback", true);
        hitparticle.Play();
        StopMoving();
        IMove = IDash((previouspoint-transform.position).normalized*0.1f,knockbackTime, dashSpeed/2f, 0);
        StartCoroutine(IMove);

    }
    void BackFlip()
    {
        anim.SetTrigger("Flip");
    }
    IEnumerator IDash(Vector3 point,float time, float moveSpeed, float rotSpeed)
    {
        Dashing = true;
        anim.SetBool("Dashing", true);
        previouspoint = transform.position;
        rb.useGravity = false;
        Vector3 dir =new Vector3(point.x, transform.position.y, point.z);
        float count = 0;
        while (count < time*point.magnitude)
        {
            count += Time.deltaTime;
            Vector3 newDir = Vector3.RotateTowards(transform.forward, dir, rotSpeed * Time.deltaTime, 0.0f);
            transform.rotation = Quaternion.LookRotation(newDir);
            transform.position = Vector3.MoveTowards(transform.position, transform.position+dir*10, moveSpeed * Time.deltaTime);
            yield return null;
            if(count > time * point.magnitude*0.9f) anim.SetBool("Dashing", false);
        }
        Dashing = false;
        anim.SetBool("DashRight", DashRight);
        anim.SetBool("Knockback", false);
        bool front = Physics.Raycast(frontray, 0.3f);
        bool back = Physics.Raycast(backray, 0.3f);
        ground = Physics.Raycast(groundray, 0.3f);
        if (!front && back) anim.SetTrigger("Front");
        else if (!back && front) anim.SetTrigger("Back");
        else if (!back && !front && !ground) anim.SetTrigger("Fall");
        DashRight = !DashRight;
        rb.useGravity = true;
    }
    void StopMoving()
    {
        rb.useGravity = true;
        Dashing = false;
        anim.SetBool("Dashing", false);
        if (IMove!=null) StopCoroutine(IMove);
    }
    public void Aim(Vector3 point)
    {
        point.y = transform.position.y;
        float angle =  Vector3.SignedAngle(transform.forward, point,Vector3.up);
        if (angle > 90) angle = 90;
        else if (angle < -90) angle = -90;
        anim.SetFloat("Angle", angle);
        anim.SetBool("Aiming", true);
    }
    public void StopAiming()
    {
        anim.SetBool("Aiming", false);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Border"&&!anim.GetBool("Knockback"))
        {
            Knockback();
        }
    }
}
