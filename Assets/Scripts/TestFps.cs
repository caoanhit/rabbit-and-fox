﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TestFps : MonoBehaviour {
    Text fps;
    int framecount = 0;
    // Use this for initialization
    void Start () {
        Application.targetFrameRate = 60;
        fps = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () {
        framecount++;
        if (framecount > 30)
        {
            framecount = 0;
            fps.text = Mathf.Round(1.0f / Time.smoothDeltaTime).ToString();
        }

    }
}
