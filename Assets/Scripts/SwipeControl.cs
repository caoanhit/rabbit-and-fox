﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[System.Serializable]
public class VectorEvent : UnityEvent<Vector3>
{
}
public class SwipeControl : MonoBehaviour {
    public float minSwipeDistance;
    public float maxSwipeDistance;
    public VectorEvent Drag , Release;
    public UnityEvent StopDrag;

    Vector2 startpoint, endpoint;
    bool touched=false;
    bool draging = false;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButton(0))
        {
            if (!EventSystem.current.IsPointerOverGameObject())
            {
                if (Input.GetMouseButtonDown(0))
                {
                    startpoint = Input.mousePosition;
                    touched = true;
                }
                endpoint = Input.mousePosition;
                Vector2 dir = endpoint - startpoint;
                if (dir.magnitude > maxSwipeDistance*Screen.width)
                {
                    endpoint =startpoint+ dir.normalized * maxSwipeDistance*Screen.width;
                }
            }
            if (Vector3.Distance(endpoint, startpoint) > minSwipeDistance*Screen.width)
            {
                if (Drag != null)
                {
                    draging = true;
                    Vector3 point = new Vector3((endpoint - startpoint).x, 0, (endpoint - startpoint).y)/Screen.width;
                    Drag.Invoke(point);
                }
            }
            else if (StopDrag != null& draging)
            {
                draging = false;
                StopDrag.Invoke();
            }
        }
        if (Input.GetMouseButtonUp(0))
        {
            if (Vector3.Distance(endpoint, startpoint) > minSwipeDistance*Screen.width)
            {
                if (Release != null && touched)
                {
                    Vector3 point = new Vector3((endpoint - startpoint).x, 0, (endpoint - startpoint).y)/Screen.width;
                    Release.Invoke(point);
                }
            }
            touched = false;
        }
    }
}
