﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LineDrawer : MonoBehaviour {
    public Transform target;
    public float length=3;

    LineRenderer line;
	// Use this for initialization
	void Start () {
        line = GetComponent<LineRenderer>();
	}
    // Update is called once per frame
    public void Draw(Vector3 point)
    {
        point *= length;
        point.y = transform.position.y;
        line.SetPositions(new Vector3[] { target.position, target.position + point });
    }
    public void StopDraw()
    {
        line.SetPositions(new Vector3[] { Vector3.zero, Vector3.zero });
    }
}
